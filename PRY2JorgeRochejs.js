function calcular() {

    var nombre = document.getElementById("nombre").value;
	var apellido = document.getElementById("apellido").value;
    var sexo = document.getElementsByName("sexo");
	var hor_diur = document.getElementById("hor_diur").value;
	var hor_noc = document.getElementById("hor_noc").value;
    var resultado, bono_fem, bono_masc, monto_diurno, monto_nocturno, pago;

	if (nombre == "" || apellido == "" || hor_diur == "" || hor_noc == "") {
		alert ("Algunos Campos Estan Vacios");
        return;
        
	}
    else if (hor_diur < 0 || hor_noc < 0) {
        alert ("Ingrese Un Valor Valido");
        return;
    }
    
    bono_fem = 4500;
    bono_masc = 3000;
    
    monto_diurno = hor_diur * 400;
    monto_nocturno = hor_noc * 800;
    
    for (var i=0 ; i < sexo.length ; i++) {
        
        if (sexo[i].checked) {
            resultado = sexo[i].value;
        }
    }
    
    if (resultado == 1) {
        
        pago = monto_diurno + monto_nocturno + bono_fem;
        
        document.getElementById("mos_nombre").innerHTML=nombre;
        document.getElementById("mos_apellido").innerHTML=apellido;
        document.getElementById("mos_bono").innerHTML=bono_fem;
        document.getElementById("mos_pago").innerHTML=pago;
            
    }
    else if (resultado == 2) {
        
        pago = monto_diurno + monto_nocturno + bono_masc;
        
        document.getElementById("mos_nombre").innerHTML=nombre;
        document.getElementById("mos_apellido").innerHTML=apellido;
        document.getElementById("mos_bono").innerHTML=bono_masc;
        document.getElementById("mos_pago").innerHTML=pago;
        
    }
    
}